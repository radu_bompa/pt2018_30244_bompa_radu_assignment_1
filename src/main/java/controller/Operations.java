package controller;

import model.Monom;
import model.Polinom;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Radu Bompa
 * 3/21/2018
 */
public class Operations {
	/**
	 * Adunarea a doua polinoame
	 * @param p1 - primul polinom
	 * @param p2 - al doilea polinom
	 * @return - polinomul suma
	 */
	public Polinom adunare(Polinom p1, Polinom p2) {
		Polinom suma = new Polinom();
		suma.setMonoms(p1.getMonoms());
		for (Monom m2 : p2.getMonoms()) {
			boolean gradAdded = false;
			for (Monom m1 : suma.getMonoms()) {
				if (m2.getGrad().equals(m1.getGrad())) {
					gradAdded = true;
					m1.setCoef(new BigDecimal(m1.getCoef().toString()).add(new BigDecimal(m2.getCoef().toString())));
				}
			}
			if (!gradAdded) {
				suma.getMonoms().add(m2);
			}
		}
		return suma;
	}

	/**
	 * Scaderea a doua polinoame
	 * @param p1 - polinomul descazut
	 * @param p2 - polinomul scazator
	 * @return - polinomul diferenta
	 */
	public Polinom scadere(Polinom p1, Polinom p2) {
		Polinom diferenta = new Polinom();
		diferenta.setMonoms(p1.getMonoms());
		for (Monom m2 : p2.getMonoms()) {
			boolean gradAdded = false;
			for (Monom m1 : diferenta.getMonoms()) {
				if (m2.getGrad().equals(m1.getGrad())) {
					gradAdded = true;
					m1.setCoef(new BigDecimal(m1.getCoef().toString()).subtract(new BigDecimal(m2.getCoef().toString())));
				}
			}
			if (!gradAdded) {
				m2.setCoef(BigDecimal.ZERO.subtract(new BigDecimal(m2.getCoef().toString())));
				diferenta.getMonoms().add(m2);
			}
		}
		return diferenta;
	}

	/**
	 * Operatia de inmultire a doua polinoame
	 * @param p1 - primul polinom
	 * @param p2 - al doilea polinom
	 * @return - polinomul rezultat
	 */
	public Polinom inmultire(Polinom p1, Polinom p2) {
		Polinom produs = new Polinom();
		Map<Integer, Number> monoame = new LinkedHashMap<>();
		for (Monom m1 : p1.getMonoms()) {
			for (Monom m2 : p2.getMonoms()) {
				int grad = m1.getGrad() + m2.getGrad();
				Number coef = new BigDecimal(m1.getCoef().toString()).multiply(new BigDecimal(m2.getCoef().toString()));
				if (monoame.get(grad) == null) {
					monoame.put(grad, coef);
				} else {
					monoame.put(grad, new BigDecimal(monoame.get(grad).toString()).add(new BigDecimal(coef.toString())));
				}
			}
		}
		List<Monom> monoamePolinom = new LinkedList<>();
		monoame.forEach((grad, coef) -> {
			Monom monom1 = new Monom();
			monom1.setGrad(grad);
			monom1.setCoef(coef);
			monoamePolinom.add(monom1);
		});
		produs.setMonoms(monoamePolinom);
		return produs;
	}

	/**
	 * Operatia de impartire a doua polinoame
	 * @param p1 - polinomul deimpartit
	 * @param p2 - polinomul impartitor
	 * @return - un Map cu o singura intrare, unde cheia e catul si valoarea e restul
	 */
	public Map<Polinom, Polinom> impartire(Polinom p1, Polinom p2) {
		/*
		* function p1 / p2:
		  require p2 != 0
		  q <- 0
		  r <- p1       # At each step p1= p2 * q + r
		  while r != 0 AND degree(r) >= degree(p2):
		     t <- lead(r)/lead(p2)     # Divide the leading terms
		     q <- q + t
		     r <- r − t * p2
		  return (q, r)
		  */
		Polinom q = new Polinom();
		List<Monom> monoameQ = new LinkedList<>();
		Monom m = new Monom();
		m.setGrad(0);
		m.setCoef(0);
		monoameQ.add(m);
		q.setMonoms(monoameQ);
		Polinom r = new Polinom();
		r.setMonoms(p1.getMonoms());
		while (r.getMonoms().size() > 0 && (getHighestDegree(r).getGrad() > getHighestDegree(p2).getGrad())) {
			Monom monomT = impartireMonoame(getHighestDegree(r), getHighestDegree(p2));
			Polinom t = new Polinom();
			List<Monom> monoameT = new LinkedList<>();
			monoameT.add(monomT);
			t.setMonoms(monoameT);
			q = adunare(q, t);
			r = scadere(r, inmultire(t, p2));

		}
		Map<Polinom, Polinom> result = new LinkedHashMap<>();
		result.put(q, r);
		return result;
	}

	/**
	 * Operatie de derivare a unui polinom
	 * @param p - polinomul care va fi derivat
	 * @return un polinom obtinut in urma derivarii
	 */
	public Polinom derivare(Polinom p) {
		Polinom derivat = new Polinom();
		List<Monom> monoameDerivate = new LinkedList<>();
		for (Monom monom : p.getMonoms()) {
			if (monom.getGrad() > 0) {
				Monom m = new Monom();
				m.setGrad(monom.getGrad() - 1);
				m.setCoef(new BigDecimal(monom.getCoef().toString()).multiply(new BigDecimal(monom.getGrad())));
				monoameDerivate.add(m);
			}
		}
		derivat.setMonoms(monoameDerivate);
		return derivat;
	}

	/**
	 * Operatie de integrare a unui polinom
	 * @param p - polinomul care va fi integrat
	 * @return un polinom obtinut in urma integrat
	 */
	public Polinom integrare(Polinom p) {
		Polinom derivat = new Polinom();
		List<Monom> monoameDerivate = new LinkedList<>();
		for (Monom monom : p.getMonoms()) {
			Monom m = new Monom();
			m.setGrad(monom.getGrad() + 1);
			m.setCoef(new BigDecimal(monom.getCoef().toString()).divide(new BigDecimal((monom.getGrad() + 1)), 2, RoundingMode.HALF_UP));
			monoameDerivate.add(m);
		}
		derivat.setMonoms(monoameDerivate);
		return derivat;
	}

	/**
	 * Metoda de obtinere a monomului de la cel mai mare grad al unui polinom
	 * @param p - polinomul pentru care calculam
	 * @return - monomul rezultat
	 */
	private Monom getHighestDegree(Polinom p) {
		Monom highestGrad = new Monom();
		highestGrad.setGrad(0);
		for (Monom monom : p.getMonoms()) {
			if (monom.getGrad() > highestGrad.getGrad() && new BigDecimal(monom.getCoef().toString()).compareTo(BigDecimal.ZERO) != 0) {
				highestGrad = monom;
			}
		}
		return highestGrad;
	}

	/**
	 * Operatie de impartire monoame
	 * @param m1 - monom deimpartit
	 * @param m2 - monom impartitor
	 * @return monom rezultat
	 */
	private Monom impartireMonoame(Monom m1, Monom m2) {
		Monom result = new Monom();
		result.setCoef(new BigDecimal(m1.getCoef().toString()).divide(new BigDecimal(m2.getCoef().toString()), 2, RoundingMode.HALF_UP));
		result.setGrad(m1.getGrad() - m2.getGrad());
		return result;
	}
}
