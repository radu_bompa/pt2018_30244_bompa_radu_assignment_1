package view;

import controller.Operations;
import model.Monom;
import model.Polinom;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by Radu Bompa
 * 3/6/2018
 */
public class MainWindow extends JPanel {

	protected static final JFrame f = new JFrame();//fereastra principala

	public MainWindow() {
		super(new GridLayout(1, 1));

		JTabbedPane tabbedPane = new JTabbedPane();

		JComponent panel1 = makeTwoPolinomsPanel("ADUNARE");
		tabbedPane.addTab("Adunare", null, panel1,
				"Adunarea a doua polinoame");
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);

		JComponent panel2 = makeTwoPolinomsPanel("SCADERE");
		tabbedPane.addTab("Scadere", null, panel2,
				"Scaderea a doua polinoame");
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);

		JComponent panel3 = makeTwoPolinomsPanel("INMULTIRE");
		tabbedPane.addTab("Inmultire", null, panel3,
				"Inmultirea a doua polinoame");
		tabbedPane.setMnemonicAt(2, KeyEvent.VK_3);

		JComponent panel4 = makeTwoPolinomsPanel("IMPARTIRE");
		panel4.setPreferredSize(new Dimension(610, 150));
		tabbedPane.addTab("Impartire", null, panel4,
				"Impartirea a doua polinoame");
		tabbedPane.setMnemonicAt(3, KeyEvent.VK_4);

		JComponent panel5 = makeOnePolynomPanel("DERIVARE");
		panel5.setPreferredSize(new Dimension(610, 150));
		tabbedPane.addTab("Derivare", null, panel5,
				"Derivarea unui polinom");
		tabbedPane.setMnemonicAt(4, KeyEvent.VK_5);

		JComponent panel6 = makeOnePolynomPanel("INTEGRARE");
		panel6.setPreferredSize(new Dimension(610, 150));
		tabbedPane.addTab("Integrare", null, panel6,
				"Integrarea unui polinom");
		tabbedPane.setMnemonicAt(5, KeyEvent.VK_6);

		//Add the tabbed pane to this panel.
		add(tabbedPane);

		//The following line enables to use scrolling tabs.
		tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
	}

	/**
	 * Metoda care afiseaza fereastra principala
	 */
	private static void createAndShowGUI() {
		//Add content to the window.
		f.add(new MainWindow(), BorderLayout.CENTER);
		//casute pentru gradele polinoamelor
		//casute pentru coeficienti
		//butoane pentru efectuarea operatiilor

		//Display the window.
		f.pack();
		f.setVisible(true);//making the frame visible
	}

	public static void main(String[] args) {

		//Schedule a job for the event dispatch thread:
		//creating and showing this application's GUI.
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				//Turn off metal's use of bold fonts
				UIManager.put("swing.boldMetal", Boolean.FALSE);
				createAndShowGUI();
			}
		});
	}

	/**
	 * Metoda ce creeaza un tab cu operatii pentru un singur polinom
	 * @param name - numele operatiei care va efectuata cu acel polinom (DERIVARE, INTEGRARE)
	 * @return - un JComponent cu panoul operatiei
	 */
	private static JComponent makeOnePolynomPanel(String name) {
		final JPanel panel = new JPanel(false);
		panel.setName(name);
		JLabel labelGradPolinom1 = new JLabel("Grad polinom 1");
		labelGradPolinom1.setHorizontalAlignment(JLabel.LEFT);
		final JTextField textPolinom1 = new JTextField();
		textPolinom1.setHorizontalAlignment(JLabel.LEFT);
		JButton buttonPolinom1 = new JButton("Coeficienti >>");

		final JLabel labelPolinom1 = new JLabel("Polinom 1");
		JButton buttonCalculeaza = new JButton("Calculeaza");

		panel.setLayout(new GridBagLayout());
		final GridBagConstraints c = new GridBagConstraints();
		c.weightx = 0.3;
		c.gridx = 0;
		c.gridy = 0;
		panel.add(labelGradPolinom1, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1;
		c.gridx = 1;
		c.gridy = 0;
		panel.add(textPolinom1, c);

		c.fill = GridBagConstraints.NONE;
		c.weightx = 0.3;
		c.gridx = 2;
		c.gridy = 0;
		panel.add(buttonPolinom1, c);

		c.weightx = 0.3;
		c.gridx = 0;
		c.gridy = 3;
		panel.add(labelPolinom1, c);

		c.weightx = 0.3;
		c.gridx = 0;
		c.gridy = 5;
		panel.add(buttonCalculeaza, c);

		/* Action listener pentru butonul de afisare a coeficientilor polinomului */
		buttonPolinom1.addActionListener(e -> {
			try {
				int coeficient = Integer.parseInt(textPolinom1.getText());
				Component[] componentList = panel.getComponents();
				for (Component c1 : componentList) {
					if (c1 instanceof JPanel) {
						panel.remove(c1);
					}
				}

				JPanel coeficienti = new JPanel();
				coeficienti.setLayout(new GridBagLayout());
				for (int i = 0; i <= coeficient * 2; i += 2) {
					JLabel labelCoeficient = new JLabel("x^" + ((coeficient * 2 - i) / 2));
					labelCoeficient.setHorizontalAlignment(JLabel.LEFT);
					JTextField valoare = new JTextField();
					valoare.setHorizontalAlignment(JLabel.LEFT);
					valoare.setName("coef_" + ((coeficient * 2 - i) / 2));

					c.gridwidth = GridBagConstraints.BOTH;
					c.weightx = 0.3;
					c.gridx = i;
					c.gridy = 0;
					coeficienti.add(labelCoeficient, c);
					c.weightx = 1;
					c.gridx = i + 1;
					c.fill = GridBagConstraints.HORIZONTAL;
					coeficienti.add(valoare, c);
					c.fill = GridBagConstraints.NONE;
				}

				c.fill = GridBagConstraints.HORIZONTAL;
				c.weightx = 1;
				c.gridx = 0;
				c.gridy = 4;
				c.gridwidth = GridBagConstraints.REMAINDER;
				panel.add(coeficienti, c);
				panel.revalidate();
				panel.repaint();
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(new JFrame(), "Coeficientul trebuie sa fie numar natural!");
			}
		});

		/* Action listener pentru butonul de efectuare a operatiei */
		buttonCalculeaza.addActionListener(e -> {
			Operations operations = new Operations();
			Polinom p = new Polinom();
			java.util.List<Monom> monoameP = new LinkedList<>();
			Component[] componentList = panel.getComponents();
			for (Component c1 : componentList) {
				if (c1 instanceof JPanel) {
					Component[] coeficientiList = ((JPanel) c1).getComponents();
					for (Component coef : coeficientiList) {
						if (coef instanceof JTextField)
							if (coef.getName().contains("coef_")) {
								Monom m = new Monom();
								m.setGrad(Integer.parseInt(coef.getName().replace("coef_", "")));
								m.setCoef(new BigDecimal(((JTextField) coef).getText()));
								monoameP.add(m);
							}
					}
				}
			}
			p.setMonoms(monoameP);

			c.fill = GridBagConstraints.HORIZONTAL;
			c.weightx = 1;
			c.gridx = 0;
			c.gridy = 8;
			switch (panel.getName()) {
				case "INTEGRARE":
					try {
						for (Component c1 : componentList) {
							if (c1 instanceof JLabel) {
								if ("RESULT_INTEGRARE".equals(c1.getName())) {
									panel.remove(c1);
								}
							}
						}
					} catch (Exception e1) {
//
					}
					JLabel resultIntegrare = new JLabel(operations.integrare(p).toString());
					resultIntegrare.setName("RESULT_INTEGRARE");
					resultIntegrare.setHorizontalAlignment(JLabel.LEFT);
					panel.add(resultIntegrare);
					panel.revalidate();
					panel.repaint();
					break;
				case "DERIVARE":
					try {
						for (Component c1 : componentList) {
							if (c1 instanceof JLabel) {
								if ("RESULT_DERIVARE".equals(c1.getName())) {
									panel.remove(c1);
								}
							}
						}
					} catch (Exception e1) {
//
					}
					JLabel resultDerivare = new JLabel(operations.derivare(p).toString());
					resultDerivare.setName("RESULT_DERIVARE");
					resultDerivare.setHorizontalAlignment(JLabel.LEFT);
					panel.add(resultDerivare);
					panel.revalidate();
					panel.repaint();
					break;
				default:
					break;
			}
		});
		return panel;
	}

	/**
	 * Metoda ce creeaza un tab pentru operatii cu doua polinoame
	 * @param name - numele operatiei care va efectuata cu acele polinoame (ADUNARE, SCADERE, INMULTIRE, IMPARTIRE)
	 * @return - un JComponent cu panoul operatiei
	 */
	private static JComponent makeTwoPolinomsPanel(String name) {
		final JPanel panel = new JPanel(false);
		panel.setName(name);
		JLabel labelGradPolinom1 = new JLabel("Grad polinom 1");
		labelGradPolinom1.setHorizontalAlignment(JLabel.LEFT);
		final JTextField textPolinom1 = new JTextField();
		textPolinom1.setHorizontalAlignment(JLabel.LEFT);
		JButton buttonPolinom1 = new JButton("Coeficienti >>");

		JLabel labelPolinom1 = new JLabel("Polinom 1");
		JLabel labelPolinom2 = new JLabel("Polinom 2");

		JLabel labelGradPolinom2 = new JLabel("Grad polinom 2");
		labelGradPolinom2.setHorizontalAlignment(JLabel.LEFT);
		final JTextField textPolinom2 = new JTextField();
		JButton buttonPolinom2 = new JButton("Coeficienti >>");

		JButton buttonCalculeaza = new JButton("Calculeaza");

		panel.setLayout(new GridBagLayout());
		final GridBagConstraints c = new GridBagConstraints();
		c.weightx = 0.3;
		c.gridx = 0;
		c.gridy = 0;
		panel.add(labelGradPolinom1, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1;
		c.gridx = 1;
		c.gridy = 0;
		panel.add(textPolinom1, c);

		c.fill = GridBagConstraints.NONE;
		c.weightx = 0.3;
		c.gridx = 2;
		c.gridy = 0;
		panel.add(buttonPolinom1, c);

		c.weightx = 0.3;
		c.gridx = 0;
		c.gridy = 1;
		panel.add(labelGradPolinom2, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 1;
		panel.add(textPolinom2, c);

		c.fill = GridBagConstraints.NONE;
		c.weightx = 0.3;
		c.gridx = 2;
		c.gridy = 1;
		panel.add(buttonPolinom2, c);

		c.weightx = 0.3;
		c.gridx = 0;
		c.gridy = 3;
		panel.add(labelPolinom1, c);

		c.weightx = 0.3;
		c.gridx = 0;
		c.gridy = 5;
		panel.add(labelPolinom2, c);

		c.weightx = 0.3;
		c.gridx = 0;
		c.gridy = 7;
		panel.add(buttonCalculeaza, c);
		/* Action listener pentru butonul de afisare a coeficientilor primului polinom */
		buttonPolinom1.addActionListener(e -> {
			try {
				int coeficient = Integer.parseInt(textPolinom1.getText());
				Component[] componentList = panel.getComponents();
				for (Component c12 : componentList) {
					if (c12 instanceof JPanel) {
						if ("coefPol1".equals(c12.getName())) {
							panel.remove(c12);
						}
					}
				}

				JPanel coeficienti = new JPanel();
				coeficienti.setName("coefPol1");
				coeficienti.setLayout(new GridBagLayout());
				for (int i = 0; i <= coeficient * 2; i += 2) {
					JLabel labelCoeficient = new JLabel("x^" + ((coeficient * 2 - i) / 2));
					labelCoeficient.setHorizontalAlignment(JLabel.LEFT);
					JTextField valoare = new JTextField();
					valoare.setHorizontalAlignment(JLabel.LEFT);
					valoare.setName("coef_" + ((coeficient * 2 - i) / 2));

					c.gridwidth = GridBagConstraints.BOTH;
					c.weightx = 0.3;
					c.gridx = i;
					c.gridy = 0;
					coeficienti.add(labelCoeficient, c);
					c.weightx = 1;
					c.gridx = i + 1;
					c.fill = GridBagConstraints.HORIZONTAL;
					coeficienti.add(valoare, c);
					c.fill = GridBagConstraints.NONE;
				}

				c.fill = GridBagConstraints.HORIZONTAL;
				c.weightx = 1;
				c.gridx = 0;
				c.gridy = 4;
				c.gridwidth = GridBagConstraints.REMAINDER;
				panel.add(coeficienti, c);
				panel.revalidate();
				panel.repaint();
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(new JFrame(), "Coeficientul trebuie sa fie numar natural!");
			}
		});

		/* Action listener pentru butonul de afisare a coeficientilor celui de-al doilea polinom */
		buttonPolinom2.addActionListener(e -> {
			try {
				int coeficient = Integer.parseInt(textPolinom2.getText());
				Component[] componentList = panel.getComponents();
				for (Component c1 : componentList) {
					if (c1 instanceof JPanel) {
						if ("coefPol2".equals(c1.getName())) {
							panel.remove(c1);
						}
					}
				}

				JPanel coeficienti = new JPanel();
				coeficienti.setName("coefPol2");
				coeficienti.setLayout(new GridBagLayout());
				for (int i = 0; i <= coeficient * 2; i += 2) {
					JLabel labelCoeficient = new JLabel("x^" + ((coeficient * 2 - i) / 2));
					labelCoeficient.setHorizontalAlignment(JLabel.LEFT);
					JTextField valoare = new JTextField();
					valoare.setHorizontalAlignment(JLabel.LEFT);
					valoare.setName("coef_" + ((coeficient * 2 - i) / 2));

					c.gridwidth = GridBagConstraints.BOTH;
					c.weightx = 0.3;
					c.gridx = i;
					c.gridy = 0;
					coeficienti.add(labelCoeficient, c);
					c.weightx = 1;
					c.gridx = i + 1;
					c.fill = GridBagConstraints.HORIZONTAL;
					coeficienti.add(valoare, c);
					c.fill = GridBagConstraints.NONE;
				}

				c.fill = GridBagConstraints.HORIZONTAL;
				c.weightx = 1;
				c.gridx = 0;
				c.gridy = 6;
				c.gridwidth = GridBagConstraints.REMAINDER;
				panel.add(coeficienti, c);
				panel.revalidate();
				panel.repaint();
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(new JFrame(), "Coeficientul trebuie sa fie numar natural!");
			}
		});
		/* Action listener pentru butonul de efectuare a operatiei */
		buttonCalculeaza.addActionListener(e -> {
			Operations operations = new Operations();
			Polinom p1 = new Polinom();
			java.util.List<Monom> monoameP1 = new LinkedList<>();
			Polinom p2 = new Polinom();
			java.util.List<Monom> monoameP2 = new LinkedList<>();
			Component[] componentList = panel.getComponents();
			for (Component c1 : componentList) {
				if (c1 instanceof JPanel) {
					if ("coefPol1".equals(c1.getName())) {
						Component[] coeficientiList = ((JPanel) c1).getComponents();
						for (Component coef : coeficientiList) {
							if (coef instanceof JTextField)
								if (coef.getName().contains("coef_")) {
									Monom m = new Monom();
									m.setGrad(Integer.parseInt(coef.getName().replace("coef_", "")));
									m.setCoef(new BigDecimal(((JTextField) coef).getText()));
									monoameP1.add(m);
								}
						}
					}
					if ("coefPol2".equals(c1.getName())) {
						Component[] coeficientiList = ((JPanel) c1).getComponents();
						for (Component coef : coeficientiList) {
							if (coef instanceof JTextField)
								if (coef.getName().contains("coef_")) {
									Monom m = new Monom();
									m.setGrad(Integer.parseInt(coef.getName().replace("coef_", "")));
									m.setCoef(new BigDecimal(((JTextField) coef).getText()));
									monoameP2.add(m);
								}
						}
					}
				}
			}

			p1.setMonoms(monoameP1);
			p2.setMonoms(monoameP2);

			c.fill = GridBagConstraints.HORIZONTAL;
			c.weightx = 1;
			c.gridx = 0;
			c.gridy = 12;
			switch (panel.getName()) {
				case "ADUNARE":
					try {
						for (Component c1 : componentList) {
							if (c1 instanceof JLabel) {
								if ("RESULT_ADUNARE".equals(c1.getName())) {
									panel.remove(c1);
								}
							}
						}
					} catch (Exception e1) {
//
					}
					JLabel resultAdunare = new JLabel(operations.adunare(p1, p2).toString());
					resultAdunare.setName("RESULT_ADUNARE");
					resultAdunare.setHorizontalAlignment(JLabel.LEFT);
					panel.add(resultAdunare);
					panel.revalidate();
					panel.repaint();
					break;
				case "SCADERE":
					try {
						for (Component c1 : componentList) {
							if (c1 instanceof JLabel) {
								if ("RESULT_SCADERE".equals(c1.getName())) {
									panel.remove(c1);
								}
							}
						}
					} catch (Exception e1) {
//
					}
					JLabel resultScadere = new JLabel(operations.scadere(p1, p2).toString());
					resultScadere.setName("RESULT_SCADERE");
					resultScadere.setHorizontalAlignment(JLabel.LEFT);
					panel.add(resultScadere);
					panel.revalidate();
					panel.repaint();
					break;
				case "INMULTIRE":
					try {
						for (Component c1 : componentList) {
							if (c1 instanceof JLabel) {
								if ("RESULT_INMULTIRE".equals(c1.getName())) {
									panel.remove(c1);
								}
							}
						}
					} catch (Exception e1) {
//
					}
					JLabel resultInmultire = new JLabel(operations.inmultire(p1, p2).toString());
					resultInmultire.setName("RESULT_INMULTIRE");
					resultInmultire.setHorizontalAlignment(JLabel.LEFT);
					panel.add(resultInmultire);
					panel.revalidate();
					panel.repaint();
					break;
				case "IMPARTIRE":
					try {
						for (Component c1 : componentList) {
							if (c1 instanceof JLabel) {
								if ("RESULT_IMPARTIRE".equals(c1.getName())) {
									panel.remove(c1);
								}
							}
						}
					} catch (Exception e1) {
//
					}
					Map<Polinom, Polinom> rezultatImpartire = operations.impartire(p1, p2);
					final Polinom[] cat = new Polinom[1];
					final Polinom[] rest = new Polinom[1];
					rezultatImpartire.forEach((k, v) -> {
						cat[0] = k;
						rest[0] = v;
					});
					JLabel resultImpartire = new JLabel("Catul: " + cat[0].toString() + ", rest: " + rest[0].toString());
					resultImpartire.setName("RESULT_IMPARTIRE");
					resultImpartire.setHorizontalAlignment(JLabel.LEFT);
					panel.add(resultImpartire);
					panel.revalidate();
					panel.repaint();
					break;
				default:
					break;
			}
		});
		return panel;
	}
}
