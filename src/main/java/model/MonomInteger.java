package model;

/**
 * Created by Radu Bompa
 * 3/6/2018
 */
public class MonomInteger extends Monom {
	private Integer coef;

	public Integer getCoef() {
		return coef;
	}

	public void setCoef(Integer coef) {
		this.coef = coef;
	}

	public String toString() {
		return coef + "* x^" + getGrad();
	}
}
