package model;

/**
 * Clasa Monom din care vom forma un Polinom
 * Created by Radu Bompa
 * 3/6/2018
 */
public class Monom {
	/*coeficientul monomului*/
	private Number coef;
	/*gradul monomului*/
	protected Integer grad;

	public Number getCoef() {
		return coef;
	}

	public void setCoef(Number coef) {
		this.coef = coef;
	}

	public Integer getGrad() {
		return grad;
	}

	public void setGrad(Integer grad) {
		this.grad = grad;
	}
}
