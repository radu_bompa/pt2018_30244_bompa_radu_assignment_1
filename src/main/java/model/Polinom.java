package model;

import java.math.BigDecimal;
import java.util.List;

/**
 * Clasa Polinom formata dintr-o lista de Monoame
 * Created by Radu Bompa
 * 3/6/2018
 */
public class Polinom {
	private List<Monom> monoms;

	public Polinom() {
	}

	public List<Monom> getMonoms() {
		return monoms;
	}

	public void setMonoms(List<Monom> monoms) {
		this.monoms = monoms;
	}

	public String toString() {
		String polinom = "";
		for (int i = 0; i < monoms.size(); i++) {
			Monom monom = monoms.get(i);
			if (new BigDecimal(monom.getCoef().toString()).compareTo(BigDecimal.ZERO) != 0) {
				polinom += monom.getCoef() + " * x^" + monom.getGrad();
				if (i < monoms.size() - 1) {
					polinom += " + ";
				}
			}
		}
		return polinom;
	}
}
