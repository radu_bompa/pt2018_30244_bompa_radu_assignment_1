package model;

/**
 * Created by Radu Bompa
 * 3/6/2018
 */
public class MonomReal extends Monom {
	private Double coef;

	public Double getCoef() {
		return coef;
	}

	public void setCoef(Double coef) {
		this.coef = coef;
	}

	public String toString() {
		return coef + "* x^" + getGrad();
	}
}
