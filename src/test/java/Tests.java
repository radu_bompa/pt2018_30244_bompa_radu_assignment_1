import controller.Operations;
import model.Monom;
import model.Polinom;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by Radu Bompa
 * 3/21/2018
 */
public class Tests {

	private Polinom p1;
	private Polinom p2;
	@Before
	public void init() {
		p1 = new Polinom();
		List<Monom> p1Monoms = new LinkedList<>();
		Monom monom = new Monom();
		monom.setGrad(0);
		monom.setCoef(1);
		p1Monoms.add(monom);
		monom = new Monom();
		monom.setGrad(1);
		monom.setCoef(2);
		p1Monoms.add(monom);
		monom = new Monom();
		monom.setGrad(2);
		monom.setCoef(-5);
		p1Monoms.add(monom);
		p1.setMonoms(p1Monoms);
		p2 = new Polinom();
		List<Monom> p2Monoms = new LinkedList<>();
		monom = new Monom();
		monom.setGrad(0);
		monom.setCoef(4);
		p2Monoms.add(monom);
		monom = new Monom();
		monom.setGrad(1);
		monom.setCoef(1);
		p2Monoms.add(monom);
		p2.setMonoms(p2Monoms);
	}

	/**
	 * Teste pentru operatia de Adunare
	 */
	@Test
	public void testAdunare() {
		Operations operations = new Operations();
		assertEquals(5.0, operations.adunare(p1, p2).getMonoms().get(0).getCoef().doubleValue());
		assertEquals(4.0, operations.adunare(p1, p2).getMonoms().get(1).getCoef().doubleValue());
		assertEquals(-5.0, operations.adunare(p1, p2).getMonoms().get(2).getCoef().doubleValue());
	}

	/**
	 * Teste pentru operatia de Scadere
	 */
	@Test
	public void testScadere() {
		Operations operations = new Operations();
		assertEquals(-3.0, operations.scadere(p1, p2).getMonoms().get(0).getCoef().doubleValue());
		assertEquals(0.0, operations.scadere(p1, p2).getMonoms().get(1).getCoef().doubleValue());
		assertEquals(-5.0, operations.scadere(p1, p2).getMonoms().get(2).getCoef().doubleValue());
	}

	/**
	 * Teste pentru operatia de Inmultire
	 */
	@Test
	public void testInmultire() {
		Operations operations = new Operations();
		assertEquals(4.0, operations.inmultire(p1, p2).getMonoms().get(0).getCoef().doubleValue());
		assertEquals(9.0, operations.inmultire(p1, p2).getMonoms().get(1).getCoef().doubleValue());
		assertEquals(-18.0, operations.inmultire(p1, p2).getMonoms().get(2).getCoef().doubleValue());
		assertEquals(-5.0, operations.inmultire(p1, p2).getMonoms().get(3).getCoef().doubleValue());
	}

	/**
	 * Teste pentru operatia de Impartire
	 */
	@Test
	public void testImpartire() {
		Operations operations = new Operations();
		Polinom cat = null;
		Polinom rest = null;
		for (Map.Entry<Polinom, Polinom> rezultat : operations.impartire(p1, p2).entrySet()) {
			cat = rezultat.getKey();
			rest = rezultat.getValue();
		}
		if (cat != null) {
			assertEquals(0.0, cat.getMonoms().get(0).getCoef().doubleValue());
			assertEquals(-5.0, cat.getMonoms().get(1).getCoef().doubleValue());
		}
		if (rest != null) {
			assertEquals(1.0, rest.getMonoms().get(0).getCoef().doubleValue());
			assertEquals(22.0, rest.getMonoms().get(1).getCoef().doubleValue());
			assertEquals(0.0, rest.getMonoms().get(2).getCoef().doubleValue());
		}
	}

	/**
	 * Teste pentru operatia de Integrare
	 */
	@Test
	public void testIntegrare() {
		Operations operations = new Operations();
		assertEquals(1.00, operations.integrare(p1).getMonoms().get(0).getCoef().doubleValue());
		assertEquals(1.00, operations.integrare(p1).getMonoms().get(1).getCoef().doubleValue());
		assertEquals(-1.67, operations.integrare(p1).getMonoms().get(2).getCoef().doubleValue());
	}

	/**
	 * Teste pentru operatia de Derivare
	 */
	@Test
	public void testDerivare() {
		Operations operations = new Operations();
		assertEquals(2.0, operations.derivare(p1).getMonoms().get(0).getCoef().doubleValue());
		assertEquals(-10.0, operations.derivare(p1).getMonoms().get(1).getCoef().doubleValue());
	}
}
